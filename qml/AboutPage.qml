import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

Page {
    id: aboutPage
    objectName: 'aboutPage'
    title: i18n.tr('About')

    Flickable {
        anchors.fill: parent
        contentHeight: column.height + units.gu(4)
        clip: true

        ColumnLayout {
            id: column
            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                topMargin: units.gu(2)
            }

            spacing: units.gu(1)

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: ubuntuShapeIcon.height

                Ubuntu.UbuntuShape {
                    id: ubuntuShapeIcon
                    anchors.centerIn: parent

                    width: units.gu(10)
                    height: units.gu(10)

                    image: Image {
                        source: Qt.resolvedUrl('../assets/logo.svg')

                        sourceSize {
                            width: ubuntuShapeIcon.width
                            height: ubuntuShapeIcon.height
                        }
                    }
                }
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('The Full Circle Magazine App')
                horizontalAlignment: Label.AlignHCenter
                color: Ubuntu.UbuntuColors.orange
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('App Author: Brian Douglass')
                horizontalAlignment: Label.AlignHCenter
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Magazine Editor: Ronnie Tucker')
                horizontalAlignment: Label.AlignHCenter
            }

            ListView {
                Layout.fillWidth: true
                Layout.preferredHeight: units.gu(6) * model.length

                model: [
                    { text: i18n.tr('Visit the Full Circle Magazine'), url: 'https://fullcirclemagazine.org/' },
                    { text: i18n.tr('Visit the app author\'s website'), url: 'https://bhdouglass.com' },
                    { text: i18n.tr('Donate to support Full Circle Magazine'), url: 'https://www.patreon.com/fullcirclemagazine' },
                    { text: i18n.tr('Donate to support the app author'), url: 'https://liberapay.com/bhdouglass' },
                ]

                delegate: ItemDelegate {
                    width: parent.width
                    height: units.gu(6)

                    text: modelData.text

                    onClicked: Qt.openUrlExternally(modelData.url)

                    Ubuntu.Icon {
                        anchors {
                            top: parent.top
                            right: parent.right
                            bottom: parent.bottom
                            rightMargin: units.gu(1)
                            topMargin: units.gu(1.5)
                            bottomMargin: units.gu(1.5)
                        }
                        width: height

                        name: 'next'
                    }
                }
            }
        }
    }
}

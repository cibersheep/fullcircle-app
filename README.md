# Full Circle App

The official Ubuntu Touch app for Full Circle Magazine

## Development

Build and run using [clickable](http://clickable.bhdouglass.com).

## Thanks

- Thanks to Joan CiberSheep for the refreshed app logo and slick new designs
- Thanks to Ronnie Tucker for heading up Full Circle Magazine

## Donate

If you like this Full Circle app, consider giving a small donation over at my
[Liberapay page](https://liberapay.com/bhdouglass) or by donating to
[Full Circle Magazine](https://www.patreon.com/fullcirclemagazine)

## License

App Author: [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
